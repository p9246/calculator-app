import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalculateService {

  private uriseg = 'http://localhost:3522/routes';

  constructor(private http: HttpClient) { }

  public getAnwser(calcul: string): Observable<any> {
    const URI = this.uriseg + '/calculate';
    return this.http.post(URI, {calcul})
  }
}
